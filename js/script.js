const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const container = document.getElementById("root");
let ul = document.createElement("ul");
container.append(ul);

function createItem(obj, list = ul) {
  let li = document.createElement("li");
  li.innerHTML = `${obj.name} (${obj.author}) - ${obj.price}$`;
  list.append(li);
}

function checkPropError(element, prop) {
  try {
    if (!element[prop]) {
      throw new SyntaxError("Введено неповні данні.");
    }
    return true;
  } catch (error) {
    console.log(`У об'єкті відсутня властивість ${prop}`);
  }
}

books.forEach(element => {
  if (
    checkPropError(element, "author") &&
    checkPropError(element, "name") &&
    checkPropError(element, "price")
  ) {
    createItem(element);
  }
});


